package com.example.daggerretrofitapinews.Dagger2;

import com.example.daggerretrofitapinews.ui.dashboard.DashboardFragment;
import com.example.daggerretrofitapinews.ui.home.HomeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract HomeFragment homeFragment();
    @ContributesAndroidInjector
    abstract DashboardFragment dashboardFragment();
}

package com.example.daggerretrofitapinews.Retorfit;

import com.example.daggerretrofitapinews.Retorfit.Entity.AllArticles;
import com.example.daggerretrofitapinews.Retorfit.Entity.Article;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET("v2/everything/")
    Call<AllArticles<Article>> getAllArticle(@Query("q") String q,@Query("apiKey")String apiKey);
    @GET("v2/everything?q=bitcoin&apiKey=4500b29c34e34bcfa5d98b04737d5f0e")
    Call<AllArticles<Article>> getAllArticleNoParam();
}

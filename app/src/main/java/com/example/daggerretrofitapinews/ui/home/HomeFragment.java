package com.example.daggerretrofitapinews.ui.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.example.daggerretrofitapinews.DetailInformation;
import com.example.daggerretrofitapinews.ModelAdapters.Adapter.NewAdapter;
import com.example.daggerretrofitapinews.ModelAdapters.Model.NewsModel;
import com.example.daggerretrofitapinews.ModelAdapters.MyListener;
import com.example.daggerretrofitapinews.R;
import com.example.daggerretrofitapinews.Retorfit.Entity.AllArticles;
import com.example.daggerretrofitapinews.Retorfit.Entity.Article;
import com.example.daggerretrofitapinews.Retorfit.ServiceApi;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends DaggerFragment implements MyListener {

    private Context context;
    private NewAdapter newAdapter;
    private List<Article> lists;
    private RecyclerView recyclerView;
    private View root;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout relativeLayout;
    private  boolean b = true;
    private TextView tv_error;
    private ImageView img_error;
    @Inject
    ServiceApi serviceApi;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        progressBar = root.findViewById(R.id.spin_kit);
        progressBar.setIndeterminateDrawable(new Wave());
        relativeLayout = root.findViewById(R.id.relatetiveFG);
        tv_error = root.findViewById(R.id.tv_error);
        img_error = root.findViewById(R.id.networking_error);

        recyclerView = root.findViewById(R.id.rvNews);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout = root.findViewById(R.id.swaprefresh);

        if(b){
            allArticleData(swipeRefreshLayout);
            refresh(swipeRefreshLayout);
        }else {

            relativeLayout.setBackgroundColor(Color.RED);
        }


        return root;
    }

    private void allArticleData(final SwipeRefreshLayout swipeRefreshLayout) {
        String q = "bitcoin";
        String apiKey = "4500b29c34e34bcfa5d98b04737d5f0e";
        Call<AllArticles<Article>> call = serviceApi.getAllArticle(q, apiKey);
        call.enqueue(new Callback<AllArticles<Article>>() {
            @Override
            public void onResponse(Call<AllArticles<Article>> call, Response<AllArticles<Article>> response) {
                if (response.isSuccessful()) {
                    lists = new ArrayList<>(response.body().getArticles());
                    System.out.println(lists.size());
                    System.out.println(lists.toString());
                    newAdapter = new NewAdapter(getActivity(), lists);
                    recyclerView.setAdapter(newAdapter);
                    progressBar.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    newAdapter.setClickMyLisener(HomeFragment.this);
                } else {
                    Toast.makeText(getActivity(), "InCorrect Data", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<AllArticles<Article>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    private void refresh(final SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                allArticleData(swipeRefreshLayout);
                swipeRefreshLayout.setColorSchemeColors(
                        getResources().getColor(android.R.color.holo_red_light),
                        getResources().getColor(android.R.color.holo_green_light),
                        getResources().getColor(android.R.color.holo_orange_light),
                        getResources().getColor(android.R.color.holo_red_light)
                );
            }
        });
    }

    @Override
    public void myClickListener(int p) {
        Intent intent = new Intent(context, DetailInformation.class);
        intent.putExtra("img", lists.get(p).getUrlToImage());
        intent.putExtra("author", lists.get(p).getAuthor());
        intent.putExtra("title", lists.get(p).getTitle());
        intent.putExtra("time", lists.get(p).getPublishedAt());
        intent.putExtra("content", lists.get(p).getContent());
        startActivity(intent);
    }

    private boolean checkNetworkConnectionStatus() {
        boolean wifiConnected;
        boolean mobileConnected;
        boolean status = true;
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeInfo = manager.getActiveNetworkInfo();
        //check either mobile and wifi
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            //checking wifi is connecting
            if (wifiConnected) {
            } else if (mobileConnected) {
                //checking mobile connecting
            }

            status = true;
        }
        //no network connected
        else {
            status = false;
        }
        return status;
    }

    public void showSnakBar(View rootView){
        tv_error.setVisibility(View.VISIBLE);
        img_error.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        Snackbar snackBar = Snackbar .make(rootView, "Error no internet connected...!", Snackbar.LENGTH_LONG) .
                setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        snackBar.show();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!checkNetworkConnectionStatus())
            showSnakBar(view);
        else{
            b = true;
            tv_error.setVisibility(View.GONE);
            img_error.setVisibility(View.GONE);
        }

    }


}